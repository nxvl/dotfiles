#!/bin/sh
#
# .profile
#
# Nicolas Valcarcel - 2012
# Based on Gustavo Picon's .profile: http://code.tabo.pe/dotfiles/src/tip/.profile
#
# This works with bash
#
# Sets some basic aliases, a valid PATH env for every host (no non-existant
# dirs), some sane env vars, and a nice looking PS1/PROMPT. It will also set
# the xterm title when possible.
#
# Add your local scripts in ~/.local/bin
# You can add more settings in ~/.local/profile
# Add more aliases in ~/.local/aliases


umask 022

alias df='df -h'
alias du='du -h'
alias j='jobs -l'
alias ls='ls -FG'
alias grep='grep --color=auto'
alias _pycleanjunk="find . -name '*.pyc' -or -name '*.orig' -or -name '*.swp' -or -name '*.swo' -or -name '*.log' -or -name Pyro_log -or -name '*.egg-info' | xargs rm -vRf"

# Import local aliases if present
if [ -f ~/.local/aliases ]; then
    . ~/.local/aliases
fi

# set the $PATH env, it will add only directories that actually exist in the
# system, so it's safe to add more and more dirs to the array
PATH=""
set_path() {
    for d in $@; do
        if [ -d $d ]; then
            if [ "$PATH" != "" ]; then
                PATH=$PATH:$d
            else
                PATH=$d
            fi
        fi
    done
}
set_path $HOME/bin $HOME/.local/bin \
         /opt/local/sbin /opt/local/bin /usr/gnu/bin \
         /ffp/sbin /ffp/bin \
         /sbin /bin \
         /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin \
         /usr/X11/bin /usr/games \
         /usr/usb /usr/opt/bin /usr/pkg/sbin /usr/pkg/bin

# alternates function STOLEN from Zak Johnson ;)
alternates() {
    for i in $*; do
        type $i >/dev/null 2>&1 && echo $i && return
    done
}
EDITOR=$(alternates vim vi)
PAGER=$(alternates less more)
LESS=gi
HISTCONTROL=ignoreboth
BLOCKSIZE=K
LC_ALL=en_US.UTF-8
LANG=en_US.UTF-8
HISTSIZE=1000
HISTFILESIZE=1000


export HISTFILESIZE HISTSIZE PATH EDITOR PAGER LESS HISTCONTROL LC_ALL LANG BLOCKSIZE


# end non-interactive section
[ -z "$PS1" ] && [ -z "$PROMPT" ] && return

#
# interactive sessions
#

parse_git_dirty() {
  [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "*"
}
parse_git_branch() {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/[\1$(parse_git_dirty)]/"
}
hg_prompt() {
    hg prompt "{[{branch}}{status}{update}]" 2>/dev/null
}

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
   TXT_WHITE=$(tput setaf 7)
   TXT_RED=$(tput setaf 1)
   TXT_YELLOW=$(tput setaf 3)
   TXT_GREEN=$(tput setaf 2)
   TXT_BOLD=$(tput bold)
   TXT_RESET=$(tput sgr0)
   PS1='\[$TXT_RED\]\u \
\[$TXT_WHITE\][\
\[$TXT_BOLD\]\[$TXT_YELLOW\]\w\
\[$TXT_RESET\]\[$TXT_WHITE\]] $(hg_prompt)$(parse_git_branch) \n\
\[$TXT_BOLD\]\[$TXT_GREEN\]\h\
\[$TXT_RESET\]\$ '
else
    PS1='\u [\w] $(hg_prompt)$(parse_git_branch) \n\h\$ '
fi

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

if [ -f "$HOME/.local/profile" ]; then
    . "$HOME/.local/profile"
fi
