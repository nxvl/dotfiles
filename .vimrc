source ~/.vim/py.vimrc

set background=dark
set clipboard=unnamed

set nocompatible
set laststatus=2
set encoding=utf-8
set t_Co=256
filetype off

" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
" vim +BundleInstall +qall

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'nvie/vim-flake8'
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-powerline'
Bundle 'msanders/cocoa.vim'
Bundle 'msanders/snipmate.vim'
Bundle 'kien/rainbow_parentheses.vim'
Bundle 'alfredodeza/khuno.vim'
Bundle 'kien/ctrlp.vim'

setlocal comments-=:#
setlocal comments+=f:#
syntax on
"set mouse=a
set bg=dark tabstop=4 softtabstop=4 shiftwidth=4 expandtab
set hlsearch
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h set colorcolumn=80
set smartindent
"set copyindent
set incsearch
set wildignore=*.swp,*.bak,*.pyc,*.class
set autoindent
set noswapfile
set fileencodings=utf-8,ucs-bom,default,latin1
"set cursorline
"set cursorcolumn
set showcmd
set hlsearch
set incsearch
set backspace=indent,eol,start
set ruler
set number
set scrolloff=5 "provide some context when scrolling

highlight SpecialKey ctermbg=yellow ctermfg=black guibg=yellow guifg=black
highlight ColorColumn ctermbg=Grey guibg=Grey
"highlight LineNr ctermfg=DarkYellow
set list
set listchars=trail:.,tab:>-,nbsp:%,extends:>,precedes:<

filetype plugin indent on

autocmd FileType python set omnifunc=pythoncomplete#Complete

let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

au BufRead /tmp/mutt-* set tw=80
" Mail colours
"hi mailHeaderKey  links to Type
"hi mailSubject    links to String
hi mailHeader     ctermfg=brown
"hi mailEmail      links to Special
hi mailSignature  ctermfg=darkblue
hi mailQuoted1    ctermfg=darkgreen
hi mailQuoted2    ctermfg=darkcyan
hi mailQuoted3    ctermfg=darkgreen
hi mailQuoted4    ctermfg=darkcyan
hi mailQuoted5    ctermfg=darkgreen
hi mailQuoted6    ctermfg=darkcyan

"nnoremap <F5> :GundoToggle<CR>

" Rainbow Parentheses
au VimEnter * RainbowParenthesesToggle
"au Syntax * RainbowParenthesesLoadRound
"au Syntax * RainbowParenthesesLoadSquare
"au Syntax * RainbowParenthesesLoadBraces

hi clear SpellBad
hi SpellBad cterm=underline,bold
